﻿using System;
using System.Collections.Generic;

namespace HanoiTowersTask
{
    /// <summary>
    /// Static class TowerOfHanoi.
    /// </summary>
    public static class HanoiTowers
    {
        /// <summary>
        /// Get count of moves to move all disks from the first tower to the third.
        /// </summary>
        /// <param name="disks">Count of disks.</param>
        /// <returns>All moves.</returns>
        public static IEnumerable<(Tower from, Tower to)> GetMoves(int disks)
        {
            if (disks <= 0 || disks > 30)
            {
                throw new ArgumentException($"{nameof(disks)} should be more than 0 and less or equal 30.");
            }

            return GetMovesRecursively(Tower.From, Tower.Buffer, Tower.To, disks);
        }

        private static IEnumerable<(Tower from, Tower to)> GetMovesRecursively(Tower fromTower, Tower bufferTower, Tower toTower, int numberOfDisksToMove)
        {
            if (numberOfDisksToMove == 1)
            {
                yield return (fromTower, toTower);
            }
            else
            {
                foreach (var element in GetMovesRecursively(fromTower, toTower, bufferTower, numberOfDisksToMove - 1))
                {
                    yield return element;
                }

                yield return (fromTower, toTower);

                foreach (var element in GetMovesRecursively(bufferTower, fromTower, toTower, numberOfDisksToMove - 1))
                {
                    yield return element;
                }
            }
        }
    }
}
