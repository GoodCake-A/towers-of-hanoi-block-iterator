﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

#pragma warning disable SA1600
#pragma warning disable CA1707

namespace HanoiTowersTask.Tests
{
    [TestFixture]
    public class TowersOfHanoiTests
    {
        private static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(1, new (Tower, Tower)[] {(Tower.From, Tower.To)});
                yield return new TestCaseData(2,
                    new (Tower, Tower)[]
                    {
                        (Tower.From, Tower.Buffer), (Tower.From, Tower.To),
                        (Tower.Buffer, Tower.To)
                    });
                yield return new TestCaseData(3,
                    new (Tower, Tower)[]
                    {
                        (Tower.From, Tower.To), (Tower.From, Tower.Buffer),
                        (Tower.To, Tower.Buffer), (Tower.From, Tower.To),
                        (Tower.Buffer, Tower.From), (Tower.Buffer, Tower.To),
                        (Tower.From, Tower.To)
                    });
            }
        }

        [TestCaseSource(nameof(DataCases))]
        public void GetMovesTests(int disks, (Tower, Tower)[] expected)
        {
            var actual = HanoiTowers.GetMoves(disks);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(31)]
        public void GetMoves_DisksLessOrEqualZeroOrMoreThanThirty_ArgumentException(int disks)
        {
            Assert.Throws<ArgumentException>(
                () => HanoiTowers.GetMoves(disks),
                message: "Count of disks should be more than 0 and less or equal 30.");
        }
        
        [TestCase(16)]
        [TestCase(20)]
        [TestCase(24)]
        public void GetMoves_WithMoreDisks_Tests(int disks)
        {
            var actual = HanoiTowers.GetMoves(disks);
            Assert.AreEqual( (Tower.From, Tower.Buffer), actual.First());
            Assert.AreEqual( (Tower.Buffer, Tower.To), actual.Last());
        }
    }
}
